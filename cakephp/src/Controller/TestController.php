<?php
   namespace App\Controller;

   use App\Controller\AppController;
   use Cake\View\Helper\SessionHelper;
   use App\Utility\OpenIDConnectClient;
   use Cake\Http\Client;
   use Cake\Core\Configure;

   class TestController extends AppController{
      public function index(){
      // get env variables
      $auth_provider = env('SSO_AUTH_PROVIDER_URL');
      $client_id = env('SSO_CLIENT_ID');
      $client_secret = env('SSO_CLIENT_SECRET');
      $client_scope = env('SSO_CLIENT_SCOPE');
      $user_profile_endpoint = env('USER_PROFILE_ENDPOINT');
      $auth_provider_cert_url = env('SSO_AUTH_PROVIDER_CERT_URL');
      $redirect_url = env('REDIRECT_URL');
      
      $oidc = new OpenIDConnectClient($auth_provider,
                                          $client_id,
                                          $client_secret);
      $oidc->setCertPath($auth_provider_cert_url);
      $oidc->setRedirectUrl($redirect_url);
      $oidc->addAuthParam(['response_mode' => 'form_post']);
      
      $oidc->authenticate();
      $userinfo = $oidc->requestUserInfo();
      $token = $oidc->getAccessToken();
      // debug($token);
      // debug($userinfo);

      //request data from user profile api
      $user_id = explode(':',$userinfo->sub)[2];
      // debug($user_id);
      $client = new Client([
         'headers' => ['Authorization' => 'Bearer ' .$token]
      ]);
      //request data from user profile api      
      $user_data_response = $client->get($user_profile_endpoint.'/'.$user_id);
      // debug($user_data_response);
      $parsed_user_data= json_decode($user_data_response->body,true);   
      //set user data in template context
      $this->set('user_data',$parsed_user_data);
      $this -> render('index');
      }
   }
?>