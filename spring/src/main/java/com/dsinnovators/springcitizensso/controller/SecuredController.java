package com.dsinnovators.springcitizensso.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

@Controller
public class SecuredController {

    @GetMapping("/securedpage")
    public String securedpage(Principal principal, Model model) {
        model.addAttribute("message", "secured page");
        model.addAttribute("username", principal.getName());
        return "securedpage";
    }
}
