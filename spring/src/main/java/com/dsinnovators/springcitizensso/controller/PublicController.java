package com.dsinnovators.springcitizensso.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PublicController {

    @GetMapping("/")
    public String main(Model model) {
        model.addAttribute("message", "public page");
        return "publicpage";
    }
}
