<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Welcome to ISDP</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: Black;
                font-family: 'Nunito', serif;
                font-size:120%;
                font-weight: 200;
                text-align:center;
                height: 50vh;
                margin: 2;
            }

        </style>
    </head>
    <body>
    <p> CITIZEN PROFILE </p>
    <img src=<?php echo "data:image/jpeg;charset=utf-8;base64,". $user_data['attributes']['photo'] ?> 
        alt=""
        height="140px"
        width="120px"/>
    <p> ID: <?php echo $user_data['id'] ?></p>
    <p> Name(English): <?php echo $user_data['attributes']['name_en'] ?></p>
    <?php if($user_data['attributes']['name_bn'] )echo ('<p> Name(Bangla): '.$user_data['attributes']['name_bn']. '</p>') ?>
    <p> Date of birth: <?php echo $user_data['attributes']['date_of_birth'] ?></p>
    <?php foreach ($user_data['contacts'] as $contacts){
        echo ('<p>'. $contacts['key'] . ": " . $contacts['value'] . '</p>');
    }?>
    <?php foreach ($user_data['ids'] as $ids){
        echo ('<p>'. $ids['key'] . ": " . $ids['value'] . '</p>');
    }?>
    </body>
</html>
