<?php

namespace App\Http\Controllers;
use App\Controller\AppController;
use Illuminate\Http\Request;
use App\Utility\OpenIDConnectClient;
use GuzzleHttp\Client;
use Stevenmaguire;

class PagesController extends Controller{
     public function index(){
        $auth_provider = env('SSO_AUTH_PROVIDER_URL');
        $client_id = env('SSO_CLIENT_ID');
        $client_secret = env('SSO_CLIENT_SECRET');
        $client_scope = env('SSO_CLIENT_SCOPE');
        $user_profile_endpoint = env('USER_PROFILE_ENDPOINT');
        $auth_provider_cert_url = env('SSO_AUTH_PROVIDER_CERT_URL');
        $redirect_url = env('REDIRECT_URL');
        
        $oidc = new OpenIDConnectClient($auth_provider,
                                            $client_id,
                                            $client_secret);
        $oidc->setCertPath( $auth_provider_cert_url);
        $oidc->setRedirectUrl($redirect_url);
        $oidc->authenticate();
        $userinfo = $oidc->requestUserInfo();
        $token = $oidc->getAccessToken();
        $user_id = explode(':', $userinfo->sub)[2];
        $client = new Client([
                 'headers' => ['Authorization' => 'Bearer ' .$token]
                  ]);
        $user_data_response = $client->request('GET',$user_profile_endpoint.'/'.$user_id);
        $parsed_user_data= json_decode($user_data_response->getBody(),true);
        return view ('private', ['user_data'=> ($parsed_user_data)]);
        }
}
?>
