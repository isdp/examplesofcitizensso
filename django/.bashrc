export OIDC_RP_SIGN_ALGO=RS256

export OIDC_OP_JWKS_ENDPOINT=http://isdp.innovatorslab.net:30010/auth/realms/ISDP/protocol/openid-connect/certs
export client_id=django-sso-client
export client_secret=6f605e47-bad3-4363-9b65-7e34106302d4

export auth_uri=http://isdp.innovatorslab.net:30010/auth/realms/ISDP/protocol/openid-connect/auth
export OIDC_OP_TOKEN_ENDPOINT=http://isdp.innovatorslab.net:30010/auth/realms/ISDP/protocol/openid-connect/token

export OIDC_OP_USER_ENDPOINT=http://isdp.innovatorslab.net:30010/auth/realms/ISDP/protocol/openid-connect/userinfo
export LOGIN_URL=http://isdp.innovatorslab.net:30010/auth/realms/ISDP/protocol/openid-connect/auth

export LOGIN_REDIRECT_URL=http://10.0.0.246:8001/secure/

export CITIZEN_PROFILE_ENDPOINT=http://isdp.innovatorslab.net:30008/api/v1/profile/

./manage.py runserver 10.0.0.246:8001