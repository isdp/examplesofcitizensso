import jwt
import requests
import os 

from django.shortcuts import render
from django.contrib.auth.decorators import login_required

def index(request):
    return render(request, 'index.html')

@login_required
def secure(request):
    id_token = request.session['oidc_id_token']
    access_token = request.session['oidc_access_token']

    claims = jwt.decode(id_token, verify=False) 

    # getting only last part of id after (:)
    mongo_id = claims['sub'].rpartition(':')[2]

    headers = {
        'Content-Type':'application/json', 
        'Authorization': 'Bearer '+ access_token 
        }
    url_path = os.environ['CITIZEN_PROFILE_ENDPOINT'] 

    url = url_path + mongo_id
    response = requests.get(url, headers=headers)
    data = response.json()

    #converting base64 image
    photo = "data:image/jpeg;base64," + data['attributes']['photo']

    return render(request, 'secure.html', {
        'claims': claims,
        'uid': mongo_id,
        'data': data,
        'photo': photo
        }) 