###### overriding some methods of the 'auth.py' of mozilia oidc #######

import logging
import requests
import base64
import hashlib

from mozilla_django_oidc.auth import OIDCAuthenticationBackend
from django.core.exceptions import SuspiciousOperation, ImproperlyConfigured
from django.utils.module_loading import import_string
from django.utils import six
from django.utils.encoding import force_bytes, smart_text, smart_bytes

LOGGER = logging.getLogger(__name__)

def default_username_algo(sub):
    username = base64.urlsafe_b64encode(
        hashlib.sha1(force_bytes(sub)).digest()
    ).rstrip(b'=')

    return smart_text(username)

class my_oidc_verification(OIDCAuthenticationBackend):
    
    def verify_claims(self, claims):
        # print(claims)
        # print('working')
         return True

    def filter_users_by_claims(self, claims):
        sub = claims.get('sub')
        if not sub:
            return self.UserModel.objects.none()
        return self.UserModel.objects.filter(email__iexact=sub)

    def create_user(self, claims):
        """Return object for a newly created user account."""
        sub = claims.get('sub')
        username = self.get_username(claims)
        return self.UserModel.objects.create_user(username, sub)

    def get_username(self, claims):
        """Generate username based on claims."""
        username_algo = self.get_settings('OIDC_USERNAME_ALGO', None)

        if username_algo:
            if isinstance(username_algo, six.string_types):
                username_algo = import_string(username_algo)
            return username_algo(claims.get('sub'))

        return default_username_algo(claims.get('sub'))

    def get_or_create_user(self, access_token, id_token, payload):
        """Returns a User instance if 1 user is found. Creates a user if not found
        and configured to do so. Returns nothing if multiple users are matched."""

        user_info = self.get_userinfo(access_token, id_token, payload)

        claims_verified = self.verify_claims(user_info)
        if not claims_verified:
            msg = 'Claims verification failed'
            raise SuspiciousOperation(msg)

        users = self.filter_users_by_claims(user_info)

        if len(users) == 1:
            return self.update_user(users[0], user_info)
        elif len(users) > 1:
            msg = 'Multiple users returned'
            raise SuspiciousOperation(msg)
        elif self.get_settings('OIDC_CREATE_USER', True):
            user = self.create_user(user_info)
            return user
        else:
            LOGGER.debug('Login failed: No user with email %s found, and '
                         'OIDC_CREATE_USER is False')
            return None
    
    def store_tokens(self, access_token, id_token):
        """Store OIDC tokens."""
        session = self.request.session

        if self.get_settings('OIDC_STORE_ACCESS_TOKEN', False):
            session['oidc_access_token'] = access_token

        if self.get_settings('OIDC_STORE_ID_TOKEN', False):
            session['oidc_id_token'] = id_token