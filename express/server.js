const Keycloak = require("keycloak-connect");
const express = require("express");
const session = require("express-session");
const app = express();
const axios = require("axios");
var exphbs = require("express-handlebars");
app.engine("handlebars", exphbs({ defaultLayout: "main" }));
app.set("view engine", "handlebars");

const memoryStore = new session.MemoryStore();
let kcConfig = {
  serverUrl: process.env.AUTH_SERVER_URL,
  realm: process.env.REALM,
  clientId: process.env.CLIENT_ID
};
const keycloak = new Keycloak({ store: memoryStore }, kcConfig);

app.use(
  session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: true,
    store: memoryStore
  })
);
app.use(keycloak.middleware());

//unprotected route
app.get("/", function(req, res) {
  res.render("home");
});

// logout route
app.get("/logout", function(req, res) {
  return req.session.destroy();
});

app.get("/test", keycloak.protect(), async function(req, res) {
  try {
    let token = JSON.parse(req.session["keycloak-token"]).access_token;
    let userAuthInfo = await axios.post(
      `${process.env.AUTH_SERVER_URL}/realms/${
        process.env.REALM
      }/protocol/openid-connect/userinfo`,
      {},
      {
        headers: {
          Authorization: "Bearer " + token
        }
      }
    );
    let userSubId = userAuthInfo.data.sub;
    let userId = userSubId.split(":")[2];
    let citizenProfile = await axios.get(
      `${process.env.CITIZEN_PROFILE_URL}/${userId}`,
      {
        headers: {
          Authorization: "Bearer " + token
        }
      }
    );
    // console.log(citizenProfile);
    res.render("profile", {
      userData: citizenProfile.data,
      profilePic: `data:image/gif;base64,${
        citizenProfile.data.attributes.photo
      }`
    });
  } catch (error) {
    console.log("error occured");
    res.render("profile", { error: error });
  }
});

app.use(keycloak.middleware({ logout: "/" }));

const host = process.env.HOST;
const port = process.env.PORT;
app.listen(port, host, function() {
  console.log(`Listening at port : ${port}`);
});
